import React, { Component } from "react";
import { View, Text, Image, StyleSheet } from 'react-native'
import { Button, ThemeProvider } from 'react-native-elements';

class OfferDetailsScreen extends Component {
  constructor(props) {
    super(props);
    console.log('OfferDetailsScreen constructor')
    const product = this.props.navigation.getParam('product', null);
    console.log('details screen', product)

    this.state = {
      product: product
    };
  }

  static navigationOptions = {
    headerTitleStyle: { alignSelf: 'center' },
    title: 'Offer Details',
    headerStyle: {
      backgroundColor: '#BA272A',
    },
  
    headerTintColor: 'white',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  }

  render() {
    return (
      <View>
        <View style={styles.header}>
          <Image
            style={{ flex: 1, resizeMode: 'stretch' }}
            source={{ uri: this.state.product.ImageUrl }} />
        </View>
        <View style={styles.body}>
          <Text style={styles.title}>{this.state.product.Title}</Text>
          <Text style={styles.description}>{this.state.product.Description}</Text>
          <ThemeProvider theme={theme}>
            <Button
              onPress={() => { this.props.navigation.navigate('QRReader', { product: this.state.product }) }}
              buttonStyle={styles.BuyButton}
              title="Scan Offer"
              
            />
          </ThemeProvider>
        </View>
      </View>
    );
  }
}

export default OfferDetailsScreen;
const theme = {
  Button: {
    titleStyle: {
      color: 'white',
      fontWeight: 'bold',
    },
  },
};

const styles = StyleSheet.create({
  header:{
    height:200
  },
  title: {
    fontSize: 22,
    color: "black",
    fontWeight: 'bold',
    paddingTop:20,
    alignSelf: 'center'
  },
  BuyButton: {
    backgroundColor: "white",
    borderColor: 'black',
    // fontWeight: 'bold',
    borderWidth: 0.5,
    borderRadius: 30,
    height: 54,
    marginTop: 30,
    width: 150,
    justifyContent: "center",
    alignSelf: "center",
    backgroundColor: '#BA272A',
  },
  description: {
    fontSize: 16,
    
    marginTop: 10,
    textAlign: 'center',
    
  }
});