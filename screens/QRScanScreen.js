'use strict';
 
import React, { Component } from 'react';
 
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  AsyncStorage ,
  Alert,
} from 'react-native';
import firebase from './../utils/firebase';
import * as Permissions from 'expo-permissions';
import { BarCodeScanner } from 'expo-barcode-scanner';
 
class QRScanScreen extends Component {
  constructor(props) {
    super(props);

    const product = this.props.navigation.getParam('product', null);
    console.log('QR screen', product);

    this.state = {
      product: product,
      hasCameraPermission: null,
      scanned: false, 
      bloodCenters: []
    };

    firebase.firestore().collection("BloodCenteres").get()
    .then(querySnapshot => {
      var bloodCenters = [];
      querySnapshot.docs.map(doc => {
        console.log('LOG bloodCenter: ', doc.data());
        var it = doc.data();
        it.id = doc.id;
        bloodCenters.push(it);
      });
      this.setState({ bloodCenters: bloodCenters });
    });
  }

  saveOfferAsync = async (product) => {
    try {
      console.log('product saving', JSON.stringify(product));
      product.timestamp = new Date();
      product.code = 'AER-AWS-23';
      await AsyncStorage.setItem('product', JSON.stringify(product));
      console.log('product saved');
    } catch (error) {
      console.log('error saving product', product);
    }
  };

  async componentDidMount() {
    this.getPermissionsAsync();
  }

  getPermissionsAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    console.log('has permision');
    this.setState({ hasCameraPermission: status === 'granted' });
  };

  handleBarCodeScanned = ({ type, data }) => {
    this.setState({ scanned: true });

    var result = this.state.bloodCenters.find(c => {
      return c.qr === data
    });

    if (result) {
      //TODO: save data 
      console.log('offer found', this.state.product);
      Alert.alert('Oferta a fost acceptata. O vei gasi pe ecaranul de profil!');
      this.state.product.Scanned = true;
      this.saveOfferAsync(this.state.product);
      this.props.navigation.navigate('Offers');
      this.props.navigation.navigate('Home');
    }
    else {
      //TODO: show error
      console.log('offer NOT found');
      Alert.alert('Eroare', 'Oferta nu a putut fi verificata. Mai incearca.', [
        {text: 'OK', onPress: () => this.setState({ scanned: false })},
      ]);      
    }
  };

  render() {
    console.log('qr rendered' + this.state.scanned);
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'flex-end',
        }}>
        <BarCodeScanner
          onBarCodeScanned={this.state.scanned ? undefined : this.handleBarCodeScanned}
          style={StyleSheet.absoluteFillObject}
        />
      </View>
    );
  }
}
 
export default QRScanScreen;

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
});
