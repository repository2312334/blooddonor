import React, { Component } from "react";
import {
    View,
    StyleSheet,
    StatusBar,
    Image,
    Text, TouchableHighlight
} from "react-native";

class Category extends Component {
    constructor(props) {
        super(props);
        console.log('Category', props);
        console.log(props.navigation);
    }

    goToOffer = ()=>{
        console.log('go to offer');
        this.props.navigation.navigate('OfferDetails');
    }

    render() {
        return (        
            <View style={{ height: 170, width: 170, marginLeft: 25 }}>                
                    <View style={{ flex: 2 }}>
                    <TouchableHighlight onPress={()=> this.goToOffer()}>
                        <Image source={{ uri: this.props.imageUri }}
                            style={{ flex: 1, width: null, height: null, resizeMode: 'cover' }}
                        />
                        </TouchableHighlight>
                    </View>

                    <View style={{ flex: 1, alignSelf: 'center' }}>
                        <Text  onPress={()=> this.goToOffer()}> {this.props.name} </Text>
                    </View>
            </View>         
        );
    }
}
export default Category;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});